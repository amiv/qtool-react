const isProd = process.env.REACT_APP_STAGE === 'production'
const isPreview = process.env.REACT_APP_STAGE === 'preview'

export const apiUrl = isProd
  ? 'https://api.amiv.ethz.ch'
  : 'https://api-dev.amiv.ethz.ch'

let preparedOAuthId = 'Local Tool'

if (isProd) {
  preparedOAuthId = 'AMIV Admintool'
} else if (isPreview) {
  preparedOAuthId = 'AMIV Admintool (dev)'
}

export const OAuthId = preparedOAuthId

export const siteTitle = 'Admintool | AMIV an der ETH'

export const author = 'AMIV an der ETH'

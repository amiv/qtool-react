import React from 'react'
import ReactDOM from 'react-dom'
import CssBaseline from '@material-ui/core/CssBaseline'
import { ThemeProvider } from '@material-ui/core/styles'
import { Router } from '@reach/router'
import { Provider as ReduxProvider } from 'react-redux'
import App from './pages/App'
import theme from './theme'
import configureStore from './store/store'

ReactDOM.render(
  <ReduxProvider store={configureStore()}>
    <ThemeProvider theme={theme}>
      {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
      <CssBaseline />
      <Router>
        <App path="/home" />
        <App path="/" />
      </Router>
    </ThemeProvider>
  </ReduxProvider>,
  document.querySelector('#root')
)

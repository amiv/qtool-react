import {
  TRANSACTION_PENDING,
  TRANSACTION_SUCCESS,
  TRANSACTION_ERROR,
} from './constants'

const transaction = () => (dispatch, getState) => {
  const { auth: { session } = {} } = getState()
  if (!session) {
    dispatch({ type: AUTH_LOGOUT_SUCCESS })
  } else {
    dispatch({
      types: [TRANSACTION_PENDING, TRANSACTION_SUCCESS, TRANSACTION_ERROR],
      resource: 'sessions',
      itemId: session._id,
      etag: session._etag,
      method: 'DELETE',
    })
  }
}

export { transaction }

import axios from 'axios'

import { apiUrl } from 'config'
import Query from 'amiv-react-components'
import { authLogout } from './auth/actions'

/*
This below is the format the middleware accepts.
When a type is set to null, it will be ignored!
{
  types: [ACTION_PENDING, { type: ACTION_SUCCESS, additionalProp: 'andItsValue' }, ACTION_FAILED],
  resource: 'sessions',
  itemId: null,
  method: 'POST',
  query: {'ajax': true},
  data: {username, password},
  dataType: 'application/json',
  token: null,
  etag: '',
}
*/
const apiMiddleware = store => next => action => {
  // If its not an core-app async action, pass next.
  if (
    typeof action.types === 'undefined' ||
    typeof action.resource === 'undefined'
  )
    return next(action)

  // get action types
  const [pendingType, successType, errorType] = action.types

  // construct the request
  const {
    resource,
    itemId = '',
    method = 'GET',
    query = {},
    dataType = 'application/json',
    data = {},
    token = null,
    etag = '',
  } = action

  const dispatchNext = (preparedAction, additionalProps) => {
    if (!preparedAction) return
    if (typeof preparedAction === 'object') {
      next({ ...additionalProps, ...preparedAction })
    } else {
      next({ type: preparedAction, ...additionalProps })
    }
  }

  // Dispatch the pending action
  dispatchNext(pendingType)

  // Prepend prefix for requests
  const url = `${apiUrl}/${resource}/${itemId}?${Query.buildQueryString(query)}`
  const { auth } = store.getState()
  const headers = { Authorization: token || auth.token }

  let promise

  switch (method) {
    case 'GET':
      promise = axios.get(url, { headers })
      break
    case 'POST':
      promise = axios.post(url, data, {
        headers: { ...headers, 'Content-Type': dataType },
      })
      break
    case 'PATCH':
      promise = axios.patch(url, data, {
        headers: { ...headers, 'If-Match': etag, 'Content-Type': dataType },
      })
      break
    case 'DELETE':
      promise = axios.delete(url, { headers: { ...headers, 'If-Match': etag } })
      break
    default:
      return null
  }

  return promise
    .then(response => {
      // Dispatch the success action
      dispatchNext(successType, { data: response.data })

      return response.data
    })
    .catch(err => {
      // automatically logout when receiving any HTTP status code 401
      if (err.response && err.response.status === 401) {
        store.dispatch(authLogout())
      }

      // Dispatch the error action
      dispatchNext(errorType, { error: err })

      throw err
    })
}

export default apiMiddleware

import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import thunk from 'redux-thunk'

import apiMiddleware from './apiMiddleware'
import authReducer from './auth/reducer'

// preloadedState will be passed in by the plugin
export default preloadedState => {
  const reducer = combineReducers({
    auth: authReducer,
  })

  const middleware = [apiMiddleware, thunk]

  // In development, use the browser's Redux dev tools extension if installed
  const enhancers = []
  const isDevelopment = process.env.NODE_ENV === 'development'
  if (
    isDevelopment &&
    typeof window !== 'undefined' &&
    window.devToolsExtension
  ) {
    enhancers.push(window.devToolsExtension())
  }

  return createStore(
    reducer,
    preloadedState,
    compose(applyMiddleware(...middleware))
  )
}

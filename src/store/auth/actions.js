import {
  AUTH_LOGOUT_PENDING,
  AUTH_LOGOUT_SUCCESS,
  AUTH_LOGOUT_ERROR,
} from './constants'

const authLogout = () => (dispatch, getState) => {
  const { auth: { session } = {} } = getState()
  if (!session) {
    dispatch({ type: AUTH_LOGOUT_SUCCESS })
  } else {
    dispatch({
      types: [AUTH_LOGOUT_PENDING, AUTH_LOGOUT_SUCCESS, AUTH_LOGOUT_ERROR],
      resource: 'sessions',
      itemId: session._id,
      etag: session._etag,
      method: 'DELETE',
    })
  }
}

export { authLogout }

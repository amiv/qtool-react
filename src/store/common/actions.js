import Query from '../../utils/query'
import {
  LIST_PENDING,
  LIST_SUCCESS,
  LIST_ERROR,
  SET_QUERY,
  SET_FILTER_VALUE,
  SET_FILTER_VALUES,
  RESET_FILTER_VALUES,
  ITEM_PENDING,
  ITEM_SUCCESS,
  ITEM_DELETED,
  ITEM_ERROR,
} from './constants'

/**
 * Update a specific filter value.
 * @param {string} resource    api resource name
 * @param {string} props.name  name of the filter
 * @param {*}      props.value new value
 */
const setFilterValue = (resource, { name, value }) => ({
  type: SET_FILTER_VALUE(resource),
  name,
  value,
})

const setFilterValues = (resource, values) => ({
  type: SET_FILTER_VALUES(resource),
  values,
})

/**
 * Reset filter values to its initial values.
 * @param {string} resource api resource name
 */
const resetFilterValues = resource => ({ type: RESET_FILTER_VALUES(resource) })

/**
 * Update stored query and clear all depending lists
 * @param {string} resource    api resource name
 * @param {object} props.query object containing query options
 */
const setQuery = (resource, { query }) => ({ type: SET_QUERY(resource), query })

/**
 * Set query from stored filter values.
 * @param {string} resource           api resource name
 * @param {func} props.queryConverter function to convert object with filter values to a query object.
 */
const setQueryFromFilterValues = (resource, { queryConverter }) => (
  dispatch,
  getState
) => {
  const { filterValues } = getState()[resource]
  const query = queryConverter(filterValues)

  dispatch({
    type: SET_QUERY(resource),
    query,
  })
}

/**
 * Load all pages for a specific list
 * @param {string} resource       api resource name
 * @param {string} props.listName name of the desired list
 */
const listLoadAllPages = (resource, { listName }) => (dispatch, getState) => {
  const { query, baseQuery } = getState()[resource][listName]

  const preparedQuery = Query.merge(baseQuery, query)

  dispatch({
    types: [{ type: LIST_PENDING(resource), listName }, null, null],
    resource,
    method: 'GET',
    query: Query.merge(preparedQuery, { page: 1 }),
  })
    .then(response => {
      const pages = { 1: response._items }
      // save totalPages as a constant to avoid race condition with pages added during this
      // process
      const totalPages = Math.ceil(
        response._meta.total / response._meta.max_results
      )

      if (totalPages <= 1) {
        return response
      }

      // now fetch all the missing pages
      return Promise.all(
        Array.from(new Array(totalPages - 1), (x, i) => i + 2).map(pageNum =>
          dispatch({
            types: [null, null, null],
            resource,
            method: 'GET',
            query: Query.merge(preparedQuery, { page: pageNum }),
          }).then(response2 => {
            pages[pageNum] = response2._items
          })
        )
      ).then(() => {
        return {
          ...response,
          _items: [].concat(
            ...Object.keys(pages)
              .sort()
              .map(key => pages[key])
          ),
          _meta: {
            page: totalPages,
            max_results: response._meta.max_results,
            total: response._meta.total,
          },
        }
      })
    })
    .then(data => {
      dispatch({ type: LIST_SUCCESS(resource), listName, append: false, data })
    })
    .catch(err => {
      dispatch({ type: LIST_ERROR(resource), listName, error: err })
    })
}

/**
 * Load next page for a specific list
 * @param {string} resource       api resource name
 * @param {string} props.listName name of the desired list
 */
const listLoadNextPage = (resource, { listName }) => (dispatch, getState) => {
  const { query, baseQuery, totalPages, lastPageLoaded } = getState()[resource][
    listName
  ]

  if (totalPages > 0 && lastPageLoaded === totalPages) return

  dispatch({
    types: [
      { type: LIST_PENDING(resource), listName },
      { type: LIST_SUCCESS(resource), listName, append: true },
      { type: LIST_ERROR(resource), listName },
    ],
    resource,
    method: 'GET',
    query: Query.merge(baseQuery, query, { page: lastPageLoaded + 1 }),
  })
}

/**
 * Load a specific item.
 * @param {string} resource     api resource name
 * @param {string} props.itemId item id
 */
const loadItem = (resource, { id }) => ({
  types: [
    { type: ITEM_PENDING(resource), itemId: id },
    { type: ITEM_SUCCESS(resource), itemId: id },
    { type: ITEM_ERROR(resource), itemId: id },
  ],
  resource,
  itemId: id,
  method: 'GET',
})

/**
 * Post signup data of the current user for an event
 * @param {string} resource   api resource name
 * @param {object} props.data item data
 */
const postItem = (resource, { data }) => ({
  types: [null, null, null],
  resource,
  method: 'POST',
  data,
  dataType: 'application/json',
})

/**
 * Patch signup data for a specific signup.
 *
 * @param {string} resource   api resource name
 * @param {string} props.id   item id
 * @param {string} props.etag etag value for the item
 * @param {string} props.data item data to be patched
 */
const patchItem = (resource, { id, etag, data }) => ({
  types: [
    { type: ITEM_PENDING(resource), itemId: id },
    { type: ITEM_SUCCESS(resource), itemId: id },
    { type: ITEM_ERROR(resource), itemId: id },
  ],
  resource,
  itemId: id,
  method: 'PATCH',
  etag,
  data,
  dataType: 'application/json',
})

/**
 * Delete a specific signup.
 * @param {string} resource api resource name
 * @param {string} props.id item id
 * @param {string} props.etag     etag value for the signup item
 */
const deleteItem = (resource, { id, etag }) => ({
  types: [
    { type: ITEM_PENDING(resource), itemId: id },
    { type: ITEM_DELETED(resource), itemId: id },
    { type: ITEM_ERROR(resource), itemId: id },
  ],
  resource,
  itemId: id,
  method: 'DELETE',
  etag,
  dataType: 'application/json',
})

export {
  setFilterValue,
  setFilterValues,
  resetFilterValues,
  setQuery,
  setQueryFromFilterValues,
  listLoadAllPages,
  listLoadNextPage,
  loadItem,
  postItem,
  patchItem,
  deleteItem,
}

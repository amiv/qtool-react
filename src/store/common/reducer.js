import {
  LIST_PENDING,
  LIST_SUCCESS,
  LIST_ERROR,
  SET_QUERY,
  SET_FILTER_VALUE,
  SET_FILTER_VALUES,
  RESET_FILTER_VALUES,
  ITEM_PENDING,
  ITEM_SUCCESS,
  ITEM_DELETED,
  ITEM_ERROR,
} from './constants'

/** Creates the initial state for a list with the given config and query. */
const _createInitialListState = (
  { createBaseQuery, additionalProps = {}, preservedAdditionalProps = [] },
  query = {},
  previousState = {}
) => {
  const now = `${new Date().toISOString().split('.')[0]}Z`

  return {
    ...Object.assign(
      {},
      ...Object.entries(additionalProps).map(([key, value]) => {
        let finalValue = value
        if (
          preservedAdditionalProps.includes(key) &&
          previousState[key] !== undefined
        ) {
          finalValue = previousState[key]
        }
        return {
          [key]: finalValue,
        }
      })
    ),
    isPending: false,
    error: false,
    items: [],
    lastPageLoaded: 0,
    totalPages: 0,
    query,
    baseQuery: Object.freeze(createBaseQuery(now)),
  }
}

/**
 * Generate a new resource reducer
 * @param {string} resource api resource name
 * @param {object} lists    object with properties (name = list name) and their list configuration
 * @param {object} initialFilterValues initial filter values for the lists.
 */
const generateReducer = (
  resource,
  lists,
  initialFilterValues = { search: '' }
) => {
  /**
   * Generate initialState on-demand.
   * This is required as the state is dependent on the current time.
   */
  const createInitialState = () => {
    return {
      items: {},
      filterValues: initialFilterValues,
      // Adds list objects on top level
      ...Object.assign(
        ...Object.entries(lists).map(([key, config]) => ({
          [key]: _createInitialListState(config),
        }))
      ),
    }
  }

  // Actual reducer is below
  const reducer = (state, action) => {
    // eslint-disable-next-line no-param-reassign
    state = state || createInitialState()

    switch (action.type) {
      case LIST_PENDING(resource):
        return {
          ...state,
          [action.listName]: {
            ...state[action.listName],
            isPending: true,
          },
        }
      case LIST_SUCCESS(resource):
        return {
          ...state,
          items: {
            ...state.items,
            ...Object.assign(
              {},
              ...action.data._items.map(itemData => {
                const item = {}
                item[itemData._id] = {
                  data: itemData,
                  isPending: false,
                  error: false,
                }
                return item
              })
            ),
          },
          [action.listName]: {
            ...state[action.listName],
            lastPageLoaded: action.data._meta.page,
            totalPages: Math.max(
              1,
              Math.ceil(action.data._meta.total / action.data._meta.max_results)
            ),
            isPending: false,
            error: false,
            items: action.append
              ? [
                  ...state[action.listName].items,
                  ...action.data._items.map(item => item._id),
                ]
              : action.data._items.map(item => item._id),
          },
        }
      case LIST_ERROR(resource):
        return {
          ...state,
          [action.listName]: {
            ...state[action.listName],
            isPending: false,
            error: action.error,
          },
        }
      case RESET_FILTER_VALUES(resource):
        return {
          ...state,
          filterValues: initialFilterValues,
        }
      case SET_FILTER_VALUE(resource):
        return {
          ...state,
          filterValues: {
            ...state.filterValues,
            [action.name]: action.value,
          },
        }
      case SET_FILTER_VALUES(resource):
        return {
          ...state,
          filterValues: action.values,
        }
      case SET_QUERY(resource):
        return {
          ...state,
          ...Object.assign(
            {},
            ...Object.entries(lists).map(([key, config]) => {
              if (!config.useQuery) return { [key]: state[key] }
              return {
                [key]: _createInitialListState(
                  config,
                  JSON.parse(JSON.stringify(action.query)),
                  state[key]
                ),
              }
            })
          ),
        }
      case ITEM_PENDING(resource):
        return {
          ...state,
          items: {
            ...state.items,
            [action.itemId]: {
              ...state.items[action.itemId],
              isPending: true,
              error: false,
            },
          },
        }
      case ITEM_SUCCESS(resource):
        return {
          ...state,
          items: {
            ...state.items,
            [action.itemId]: {
              data: action.data,
              isPending: false,
              error: false,
            },
          },
        }
      case ITEM_DELETED(resource):
        return {
          ...state,
          items: {
            ...state.items,
            [action.itemId]: {
              data: action.data,
              isPending: false,
              error: false,
            },
          },
        }
      case ITEM_ERROR(resource):
        return {
          ...state,
          items: {
            ...state.items,
            [action.itemId]: {
              data: null,
              isPending: false,
              error: action.error,
            },
          },
        }
      default:
        return state
    }
  }

  // Append state init function for use when inheriting from this reducer
  reducer.createInitialState = createInitialState

  return reducer
}

export default generateReducer
